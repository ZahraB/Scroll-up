let scroll = document.querySelector('button.scroll')
let scroll_button = document.querySelector('button.scroll_button')
window.addEventListener('scroll', e => {
    if (window.scrollY > 300 & window.scrollY < 2000) {
        scroll.classList.add('show')
    } else {
        scroll.classList.remove('show')
    }
})
scroll.addEventListener('click', e => {
    if( scroll.classList.contains('show')) {
        window.scrollTo({top: 0, behavior: 'smooth'})
    }
})

window.addEventListener('scroll', e => {
    if (window.scrollY > 2000) {
        scroll_button.classList.add('show')
    } else {
        scroll_button.classList.remove('show')
    }
})
scroll_button.addEventListener('click', e => {
    if( scroll_button.classList.contains('show')) {
        document.querySelector('#article').scrollIntoView({top: 0, behavior: 'smooth'})
    }
})